//
//  TableViewController.swift
//  TableViewTest
//
//  Created by James Clancey on 9/20/15.
//  Copyright © 2015 iis. All rights reserved.
//


import UIKit

class TableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    var items = ["1", "2","Foo","Thomas","bar"]
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") ?? UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell");
        
        cell.textLabel?.text = items[indexPath.row]
        return cell;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = items[indexPath.row];
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ViewController") as! ViewController;
        vc.title = item;
        //vc.Label.text = item;
        //vc.Text = item;
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

